# SPDX-License-Identifier: GPL-3.0-or-later
from decimal import Decimal
from email.utils import parsedate_to_datetime
from datetime import datetime, timezone
import os.path
from pathlib import Path
import requests
import logging
import json

from arifu.region import Region
    
class Regions():
    # The following are class attributes that are modified per class
    etag = None # ETag header, eg. 'W/"5d8cbffb-530ec"'
    lastmodified = None # Region list was last modified on server
    expires = None # Server cache Expires header as datetime
    _data = None # Raw json response from server
    regions = {} # Parsed regions as List of Region
    CHANNEL_URL = "https://warnung.bund.de/assets/json/suche_channel.json"
    
    def __init__(self):
        # Determine cache dir
        data_dir = Path(os.getenv('XDG_DATA_HOME', None) or \
                        Path('~/.local/share').expanduser())
        if data_dir.is_dir():
            data_dir = data_dir / 'katwarn'
            logging.debug("Using cache dir {}".format(data_dir))
            try:
                data_dir.mkdir(exist_ok=True)
            except:
                logging.warning("Could not create cache dir {}, "
                                "not caching data! "
                                "Not good for katwarn server".format(data_dir))
                data_dir = None
        else:
            logging.warning("User data dir {} does not exist, "
                            "not caching data! "
                            "Not good for katwarn server". format(data_dir))
            data_dir = None
        self.cache_dir = data_dir
        # Load cached regions if possible
        self.load_cached_regions()

        try:
            current = self.check_for_update()
        except Exception as e:
            logging.error("Could not retrieve regions, we are not usable")
            logging.debug("Exception raised in check_for_update() was: {}".format(e))
            return

        if current:
            logging.info("Need to update our region list")
            self.do_update()
        elif current == False:
            pass # Cache of regions still current
        elif current == None:
            logging.debug("Using outdated region data")
        else:
            raise ("We should never ended up here!")

    def load_cached_regions(self):
        """Loads region list from cache file (if possible).

        It populates ._data which will still need to be parsed with
        parse_raw_regions() before it can be used.

        :returns: Nothing
        """
        ### XXX Should this be a classmethod to enable file locking?
        if self.cache_dir is None: return
        cache = self.cache_dir / 'regions.json'
        if not cache.is_file(): return
        with open(cache, "rt") as f:
            logging.debug("Loading cached regions.")
            data = json.load(f)
            self.etag = data['ETag']
            self.lastmodified = datetime.fromisoformat(data['Last-Modified'])
            self.expires = datetime.fromisoformat(data['Expires'])
            self._data = data['regions']
        self.parse_raw_regions()


    def save_regions(self):
        """Save retrieved regions to cache.

        :returns: True on success, False on failure."""
        ### XXX Should this be a classmethod to enable file locking?
        if self.cache_dir is None: return False
        cache = self.cache_dir / 'regions.json'

        ### XXX TODO Use file locking to prevent parallel writes?
        with open(cache, "tw", encoding="utf-8") as f:
            logging.debug("Caching regions.")
            json.dump({'ETag':self.etag,'Expires':self.expires.isoformat(),
                       'Last-Modified':self.lastmodified.isoformat(),
                       'regions':self._data}, f)
        return True


    def check_for_update(self):
        """Checks if we need up update our cached region list

        :returns: True is we need to update, False if we have a
            current cache and None if the status cannot be determined
            (e.g. we are offline, but have an expired cache). It raises an
            exception if we have no cached data and the server returns an
            error (or we are offline).
        """
        now = datetime.now(timezone.utc)
        if self.expires is not None and (now < self.expires):
            # Everything is fine and current, don't do anything
            logging.debug("Cached regions still current for {}".format(
                self.expires-now))
            return False

        r = None # stores the response object if we receive one        
        try:
            r = requests.head(self.CHANNEL_URL)
        except requests.ConnectionError as e:
            exc = e
            if "Temporary failure in name resolution" in str(e):
                logging.info("Could not resolve URL, are we offline?")
            else:
                logging.warning("Could not connect to URI {} to check for "
                                "regions".format(self.CHANNEL_URL))
            
            # If we are here, we could not determine the current
            # status, but there could be an expired cache to use.
            if self.expires is not None:
                # XXX TODO: write how much it is out of date and refuse too old ones
                logging.warning("Expired cache of regions is the best we have, "
                             "using that (expired for {} days).".format(
                                 (now-self.expires).days))
                return None

            # Finally, in the case of failure, raise the exception we received
            raise exc

        # Server returns 'Date' , eg. 'Tue, 18 Feb 2020 07:06:42 GMT'
        #  'ETag' header, eg. 'W/"5d8cbffb-530ec"'
        #  'Expires' header, eg. 'Tue, 25 Feb 2020 07:06:42 GMT'
        #  'Last-Modified': 'Thu, 26 Sep 2019 13:41:15 GMT'

        assert(r.status_code == 200)
        # TODO: If there is no Expires header, use the "Date" header instead?
        expires = parsedate_to_datetime(r.headers["Expires"])
        lastmodified = parsedate_to_datetime(r.headers["Last-Modified"])
        
        if "ETag" in r.headers:
            etag = r.headers["ETag"].strip("W/")
            if self.etag == etag or "Last-Modified" in r.headers and \
               self.lastmodified == lastmodified:
                logging.debug("ETag or Last-Modified match. "
                              "Region list is still current")
                # Update the "Expires information" and save to cache
                self.expires = expires
                self.save_regions()
                return False # We are up to date
        return True # need to update region list


    def do_update(self):
        """Fetch new region list from server and caches them

        This also caches the retrieved data.
        It populates ._data which will still need to be parsed with
        parse_raw_regions() before it can be used.

        :returns: True on success, False on failure.
        """
        logging.info("Updating the region list, this means region ids could "
                     "change. Verify your subscriptions")
        try:
            r = requests.get(self.CHANNEL_URL)
        except requests.exceptions.ConnectionError as e:
            logging.warning("Could not connect to URI {}: {}".format(
                self.CHANNEL_URL, e))
            
            if "Temporary failure in name resolution" in str(e):
                logging.warning("Could not resolve URL, are we offline?")
            return False
        
        if r.status_code != 200:
            logging.warn("Failed to update region channels, "
                         "error code {}".format(r.status_code))
            return False

        assert (r.headers['content-type'] == "application/json")
        # enforce utf8 encoding, as server does not tell us and we avoid probing
        r.encoding = "utf-8"
        
        # Strip initial optional W/, see
        # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag
        self.etag = r.headers["ETag"].strip("W/")

        self.expires = parsedate_to_datetime(r.headers["Expires"])
        #date = parsedate(r.headers["Date"])
        self.lastmodified = parsedate_to_datetime(r.headers["Last-Modified"])

        self._data = r.json()
        self.parse_raw_regions()
        self.save_regions()
        return True

    def parse_raw_regions(self):
        if self._data == None: return

        regions={}
        for ags, attributes in self._data.items():
            r = Region(ags, attributes)
            regions[ags] = r
        self.regions = regions

    def find_by_id(self, id: str):
        "Return Region() by its id or None"
        if id in self.regions:
                return self.regions[id]
        return None

    def find_by_name(self, name: str):
        "Return Region() by its NAME or None"
        name = name.lower()
        for id, r in self.regions.items():
            if r.name == name:
                return r
        return None

    def find_by_gemeinde(self, gemeinde: str):
        "Return Region() by a contained gemeinde with name `gemeinde` or None"
        name = gemeinde.lower()
        for id, r in self.regions.items():
            if name in r.gemeinden:
                return r
        return None

    def find_by_position(self, lonlat):
        raise NotImplemented()
