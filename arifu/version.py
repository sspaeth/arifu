# -*- coding: utf-8 -*-
# ©Sebastian Späth
# SPDX-License-Identifier: GPL-3.0-or-later


__VER__  = "0.0.0+"
__NAME__ = "arifu"
__DESC__ = """Library to access katwarn and other warnings from the BKK"""
__AUTHOR__ = "Sebastian Spaeth"
__EMAIL__ = "Sebastian@SSpaeth.de"
__LICENSE__ = "GPL-3.0-or-later"
__all__ = ["__VER__","__NAME__","__DESC__","__AUTHOR__","__EMAIL__","__LICENSE__"]
