# -*- coding: utf-8 -*-
# ©Sebastian Späth
# SPDX-License-Identifier: GPL-3.0-or-later
import logging, sys, argparse
from arifu.version import *
from arifu.regions import Regions


class ArifuApp():

    def cmdlineargs(self):
        parser = argparse.ArgumentParser(prog=__NAME__, description=__DESC__)
        parser.add_argument("-n", "--id", help=
                            "display warnings for Region identifier (AGS).")
        parser.add_argument("-r", "--region", help=
                            "display warnings for Region Name (AGS).")
        parser.add_argument("-g", "--gemeinde", help=
                            "display warnings for Gemeinde.")

        parser.add_argument("-q", "--quiet", help=
                            "Only log severe errors.",
                            action="store_true")
        parser.add_argument("-v", "--verbose", help=
                            "Enable verbose logging.",
                            action="store_true")
        parser.add_argument("-d", "--debug", help=
                            "Enable debug logging.",
                            action="store_true")
        parser.add_argument("-V", "--version", help=
                            "display the version and quit.",
                            action="store_true")
        args = parser.parse_args()

        if args.version:
            print(__VER__)
            sys.exit(0)

        # Do not log verbose "requests" and "urllib3" modules
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger("urllib3").setLevel(logging.WARNING)
        loglevel = logging.WARNING #default
        if args.verbose:
            loglevel = logging.INFO
        elif args.quiet:
            loglevel = logging.ERROR
        elif args.debug:
            loglevel = logging.DEBUG

        FORMAT = '%(asctime)-15s %(levelname)s %(message)s'
        logging.basicConfig(level=loglevel, format=FORMAT, datefmt="%y%m%d %H:%M:%S")

        self.args = args

    def run(self):
        self.cmdlineargs()
        if self.args.gemeinde:
            r = Regions()
            reg = r.find_by_gemeinde(self.args.gemeinde)
            print(str(reg), reg.check_status())
        elif self.args.region:
            r = Regions()
            reg = r.find_by_name(self.args.region)
            print(str(reg), reg.check_status())
        elif self.args.id:
            r = Regions()
            reg = r.find_by_id(self.args.id)
            print(str(reg), reg.check_status())
