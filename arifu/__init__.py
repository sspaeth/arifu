# SPDX-License-Identifier: GPL-3.0-or-later

#__VER__ __NAME__ __DESC__ __AUTHOR____EMAIL__ __LICENSE__
from arifu.version import *
from arifu.app import ArifuApp
__all__ = ["ArifuApp"]
