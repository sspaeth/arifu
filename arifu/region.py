# SPDX-License-Identifier: GPL-3.0-or-later
from decimal import Decimal
import requests
import logging

class Bucket():
    """A Bucket containing warings 

Alle Warnmeldungen mit: z.B.
https://warnung.bund.de/bbk.katwarn/warnmeldungen.json
https://warnung.bund.de/bbk.biwapp/warnmeldungen.json
http://warnung.bund.de/bbk.dwd/unwetter.json
http://warnung.bund.de/bbk.wsv/hochwasser.json
http://warnung.bund.de/bbk.lhp/hochwassermeldungen.json
http://warnung.bund.de/bbk.dwd/waldbrand.json
http://warnung.bund.de/bbk.bgr/erdbeben.json

Notable instance attributes:
 name string containing the bucketname"""
    def __init__(self):
        pass

    @classmethod
    def fromdata(cls, data):
        b = Bucket()
        b.name = data['bucketname']
        b.status = data['status']
        b.cancelCount = data['cancelCount']
        b.activeCount = data['activeCount']
        b.ref = data['ref'] # List of strings
        return b


class Region():
    """A single region which can be subscribed to

    instance attributes:
    .id (unique string identifiying the region)
    .name = lowercased name of the region
    .gemeinden: list of gemeinden within this region
    .lower_left
    .upper_right
    .etag (BKK etag in enclosed '"')
    .buckets
"""
    etag = None
    buckets = {}

    def __init__(self, id: str, attributes):
        #id must be str as 02000 != 2000
        assert(isinstance(id, str))
        self.id = id
        {'LOWERLEFT': '9.3576, 54.7519', 'UPPERRIGHT': '9.506, 54.8371',
         'GEMEINDEN': 'Flensburg'}
        self.name = attributes['NAME'].lower()
        #'12.2074, 50.8098' -> (Decimal(12.2074), Decimal(50.8098))
        self.lower_left = ([Decimal(x.strip())
                            for x in attributes['LOWERLEFT'].split(",")])
        self.upper_right = ([Decimal(x.strip())
                             for x in attributes['UPPERRIGHT'].split(",")])
        if isinstance(attributes['GEMEINDEN'], str):
            self.gemeinden = [attributes['GEMEINDEN'].lower()]
        else:
            self.gemeinden = [g.lower() for g in attributes['GEMEINDEN']]

    def check_status(self):
        """Checks a region and updates warnings

        :returns: True if we have new information, and False if we are
            still up to date and None if we could not retrieve the
            status.
        """
        
        STATUS_URL = "https://warnung.bund.de/bbk.status/status_{:<012}.json"\
            .format(self.id)
        logging.info("Checking region status at {}".format(STATUS_URL))
        # XXX TODO: Use headers["If-None-Match"] which returns 304 on no change
        headers = {}
        if self.etag:
            # We have a status cached, only update us on news
            headers['If-None-Match']=self.etag
        try:
            r = requests.get(STATUS_URL, headers=headers)
        except requests.exceptions.ConnectionError as e:
            if "Temporary failure in name resolution" in str(e):
                logging.warning("Could not resolve URL, are we offline?")
            else:
                logging.warning("Could not connect to URI {} to update region "
                                "status".format(STATUS_URL))
            return None

        if r.status_code == 304:
            logging.debug("No new status for region {}".format(self))
            return False
        elif r.status_code != 200:
            logging.warning("Failed to update region channels, "
                         "error code {}".format(r.status_code))
            return None

        """
[{"bucketname":"bbk.mowas","status":0,"cancelCount":0,"activeCount":0,"ref":[]},{"bucketname":"bbk.biwapp","status":0,"cancelCount":0,"activeCount":0,"ref":[]},{"bucketname":"bbk.katwarn","status":0,"cancelCount":0,"activeCount":0,"ref":[]},{"bucketname":"bbk.lhp","status":0,"cancelCount":0,"activeCount":0,"ref":[]},{"bucketname":"bbk.dwd","status":1,"cancelCount":0,"activeCount":1,"ref":["fdaa97ac031b683c267956194bea22658319024de541742a9f3a12107e29ea5b"]}]"""

        data = r.json()
        buckets = {}
        for data in r.json():
            b = Bucket.fromdata(data)
            buckets[b.name] = b
        self.buckets = buckets

        # Set current ETag to allow future shortcuts
        self.etag = r.headers["ETag"].strip('W/')
        return True # We have updated information

    def __str__(self):
            return "Region {} ({})".format(self.name, self.id)
