#!/usr/bin/env python3

from distutils.core import setup
with open('arifu/version.py') as f: exec(f.read())
#__import __NAME__,__VER__,__DESC__,__AUTHOR__,__EMAIL__,__LICENSE__

cmdclass = {}
try:
    from sphinx.setup_command import BuildDoc
    cmdclass = {'docs': BuildDoc}
except Exception as e:
    raise e

setup(name=__NAME__,
      version=__VER__,
      description=__DESC__,
      cmdclass=cmdclass,
      author=__AUTHOR__,
      author_email=__EMAIL__,
      url='https://sspaeth.gitlab.io/arifu',
      packages=['arifu'],
      scripts=['bin/arifu'],
      install_requires=['requests',], #external packages as dependencies
      command_options={
          'docs': {
              'project': ("setup.py", __NAME__),
              'version': ("setup.py", __VER__),
              'release': ("setup.py", __VER__),
              'build_dir': ("setup.py", "public"),}},
              #'source_dir': ("setup.py", 'docs')}},
     )
