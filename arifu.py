#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ©Sebastian Späth
# SPDX-License-Identifier: GPL-3.0-or-later
import logging

from arifu import ArifuApp

if __name__ == "__main__":
    app = ArifuApp()
    app.run()

    
    
